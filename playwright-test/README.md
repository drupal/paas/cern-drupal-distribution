# Playwright Test

This README describe how to write playwright test and debug them.
It's highly recommended to read the [playwright documentation](https://playwright.dev/docs/next/intro#introduction).

### Create a Playwright Test

Playwright tests are easy to create.
There are multiple ways to create one:

+ Write it
+ [Use Codegen (VSCode extention) to generate it](https://playwright.dev/docs/getting-started-vscode#generating-tests)

In both case you're test should have **actions** and **assertions**. 

## Debug test

If you have to debug a test you can either use the UI Mode, the report or the trace.

### UI Modenot working in local

It's higly recommend to debug your test with UI Mode
To open UI mode, run the following command in your terminal:

```
npx playwright test --ui
```

### Report

after running tests a report could be seen with the command:
```
npx playwright show-report
```
### Trace

after running tests in a CI environment, playwright will create a trace.zip file for each failed test. if you want to view the trace you can use the [trace viewer](https://trace.playwright.dev/)

If you are in local you can force the record of traces by using "--trace on"

```
npx playwright test --trace on
```

## Update Playwright

You can update Playwright by using this command 

```
npm install -D @playwright/test@latest
```