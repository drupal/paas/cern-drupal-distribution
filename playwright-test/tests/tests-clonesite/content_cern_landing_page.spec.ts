import { test, expect } from '@playwright/test';

/* Test for "CERN landing page" content:
1) Check that we can open the creation page
2) Check that we can add a title
3) Check that we can add a component
4) Check that we can add a component title
5) Check that we can add content in the component
6) Check that we can publish a CERN landing page
7) Check the published CERN landing page
*/

let CERNLandingPageTitle:string = 'this is a CERN landing page title';
let CERNLandingPageSectionTitle:string = 'this is the CERN landing page section title';
let CERNLandingPageColumnTitle:string = 'this is the CERN landing page column title';
let CERNLandingPageColumnBody:string = 'this is the CERN landing page column body';

test('test_content_cern_landing_page', async ({ page }) => {
  // 1) open the creation page 
  await page.goto('/');
  
  await page.getByRole('link', { name: 'Content', exact: true }).click();
  await page.getByRole('link', { name: '+ Add content' }).click();
  await page.getByRole('link', { name: 'Landing Page A long form story telling page.' }).click();

  // 2) add a title 
  await page.getByLabel('Title', { exact: true }).fill(CERNLandingPageTitle);

  // 3) add a component
  await page.getByRole('link', { name: 'Components' }).click();
  await page.getByRole('button', { name: 'Add Section' }).click();

  // 4) add a component title
  await page.getByRole('textbox', { name: 'Title', exact: true }).fill(CERNLandingPageSectionTitle);

  // 5) add content in the component
  await page.getByRole('link', { name: 'Content'}).filter({ has: page.getByRole('strong') }).click();

  // loop to create left, right and center column and add text in
  for (let i = 0; i < 3; i++) {
    await page.getByRole('button', { name: 'Add Text Component'}).nth(i).click();

    await page.getByRole('textbox', { name: 'Title' }).nth(i).fill(CERNLandingPageColumnTitle);
    await page.frameLocator('iframe[title="Rich Text Editor\\, Text field"]').nth(i).locator('html').click();
    await page.frameLocator('iframe[title="Rich Text Editor\\, Text field"]').nth(i).locator('body').fill(CERNLandingPageColumnBody);
  }
  // 6) publish a CERN landing page
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByLabel('Status message').isVisible();

  // 7) Check the published CERN landing page
  await expect(page.getByText(CERNLandingPageColumnTitle)).toHaveCount(3);
  await expect(page.getByText(CERNLandingPageColumnBody)).toHaveCount(3);
});

