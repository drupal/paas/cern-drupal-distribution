import { test, expect } from '@playwright/test';

/* Test for "Article" content:
1) Check that we can open the creation page
2) Check that we can add a title
3) Check that we can add a body
4) Check that we can upload an image
5) Check that we can publish an article
6) Check the published article
*/


let articleTitle:string = 'this is an article title';
let articleBody:string = 'this is the article body';
let articleaAlternativeText:string = 'alternative_text';
let articleaImagePath:string = 'tests/assets/upload-image-1200x438.png';

test('test_content_article', async ({ page }) => {
  // 1) open the creation page 
  await page.goto('/');

  await page.getByRole('link', { name: 'Content', exact: true }).click();
  await page.getByRole('link', { name: '+ Add content' }).click();
  await page.getByRole('link', { name: 'Article Use articles for time-sensitive content like news, press releases or blog posts.' }).click();

  // 2) add a title
  await page.getByLabel('Title', { exact: true }).fill(articleTitle);
  // 3) add a body
  await page.frameLocator('iframe[title="Rich Text Editor\\, Body field"]').locator('body').fill(articleBody);
  // 4) upload an image
  await page.getByRole('textbox', { name: 'Image' }).setInputFiles(articleaImagePath);
  await page.getByLabel('Alternative text').fill(articleaAlternativeText);
  // 5) publish an article
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByLabel('Status message').isVisible();
  // 6) Check the published article
  await expect(page.getByRole('heading', { name: articleTitle }).locator('span')).toHaveText(articleTitle);
  await expect(page.getByText(articleBody)).toHaveText(articleBody);
  await expect(page.getByRole('img', { name: articleaAlternativeText })).toBeVisible();
});

