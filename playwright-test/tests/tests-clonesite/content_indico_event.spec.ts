import { test, expect } from '@playwright/test';

/* Test for "Indico Event" content:
1) Check that we can open the creation page
2) Check that we can add a title
3) Check that we can add a body
4) Check that we can add date
5) Check that we can select a type
6) Check that we can add category informations
7) Check that we can add indico informations
8) Check that we can publish a Indico Event
9) Check the published Indico Event
*/

let IndicoEventTitle:string = 'this is a Indico event title';
let IndicoEventBody:string = 'this is the Indico event body';
let IndicoEventStartDate:string = '2027-06-22';
let IndicoEventEndDate:string = '2027-06-22';
let IndicoEventStartHour:string = '10:10:00';
let IndicoEventEndHour:string = '11:12:00';
let IndicoEventTimeZone:string = 'UTC';
let IndicoEventCategoryTitle:string = 'this is the Indico event category title';
let IndicoEventCategoryID:string = 'this is the Indico event category id';
let IndicoEventIndicoLink:string = 'this is the Indico event indico link';
let IndicoEventIndicoIcal:string = 'this is the Indico event indico ical';


test('test_content_indico_event', async ({ page }) => {
  // 1) open the creation page
  await page.goto('/');

  await page.getByRole('link', { name: 'Content', exact: true }).click();
  await page.getByRole('link', { name: '+ Add content' }).click();
  await page.getByRole('link', { name: 'Indico Event Used by Indico Feeds. Creating content of this type manually will NOT create an event in Indico.' }).click();


  // 2) add a title
  // await page.getByLabel('Title', { exact: true }).fill(IndicoEventTitle);

  // 3) add a body
  await page.frameLocator('iframe[title="Rich Text Editor\\, Description field"]').locator('body').fill(IndicoEventBody);

  // 4) add date
  await page.getByRole('textbox', { name: 'Date :' }).fill(IndicoEventStartDate);
  await page.getByRole('textbox', { name: 'Time :' }).fill(IndicoEventStartHour);
  await page.getByRole('textbox', { name: 'Date:' }).fill(IndicoEventEndDate);
  await page.getByRole('textbox', { name: 'Time:' }).fill(IndicoEventEndHour);
  await page.getByLabel('Timezone').fill(IndicoEventTimeZone);

  // 5) select a type of event
  await page.getByLabel('Type').selectOption('simple_event');

  // 6) add category informations
  await page.getByLabel('Category', { exact: true }).fill(IndicoEventCategoryTitle);
  await page.getByLabel('Category ID').fill(IndicoEventCategoryID);

  // 7) add Indico informations
  await page.getByLabel('Indico link').fill(IndicoEventIndicoLink);
  await page.getByLabel('Indico iCal').fill(IndicoEventIndicoIcal);

  // 8) publish a Indico Event
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByLabel('Status message').isVisible();

  // 9) Check the published Indico Event
  // await expect(page.getByRole('heading', { name: IndicoEventTitle }).locator('span')).toHaveText(IndicoEventTitle);
  await expect(page.getByText(IndicoEventBody)).toHaveText(IndicoEventBody);
  await expect(page.getByText(IndicoEventTimeZone)).toHaveText(IndicoEventTimeZone);
  await expect(page.getByText(IndicoEventCategoryTitle)).toHaveText(IndicoEventCategoryTitle);
  await expect(page.getByText(IndicoEventCategoryID)).toHaveText(IndicoEventCategoryID);
  await expect(page.getByText(IndicoEventIndicoLink)).toHaveText(IndicoEventIndicoLink);
  await expect(page.getByText(IndicoEventIndicoIcal)).toHaveText(IndicoEventIndicoIcal);
});
