import { test, expect } from '@playwright/test';


/* Test for "Webcast Event" content:
1) Check that we can open the creation page
2) Check that we can add a title
3) Check that we can add date
4) Check that we can add category informations
5) Check that we can add webcast informations
6) Check that we can add a body
7) Check that we can publish a Webcast Event
8) Check the published Webcast Event
*/

let WebcastEventTitle:string = 'this is a Webcast event title';
let WebcastEventAbstract:string = 'this is the Webcast event Abstract';
let WebcastEventStartDate:string = '2027-06-22';
let WebcastEventEndDate:string = '2027-06-22';
let WebcastEventStartHour:string = '10:10:00';
let WebcastEventEndHour:string = '11:12:00';
let WebcastEventTimeZone:string = 'UTC';
let WebcastEventCategoryTitle:string = 'this is the Webcast event category title';
let WebcastEventCategoryID:string = 'this is the Webcast event category id';
let WebcastEventWebcastLink:string = 'this is the Webcast event webcast link';
let WebcastEventWebcastIcal:string = 'this is the Webcast event webcast ical';


test('test_content_webcast_event', async ({ page }) => {
  // 1) open the creation page
  await page.goto('/');

  await page.getByRole('link', { name: 'Content', exact: true }).click();
  await page.getByRole('link', { name: '+ Add content' }).click();
  await page.getByRole('link', { name: 'Webcast Event Used by Webcast Feeds. Creating content of this type manually will NOT create an event in Webcast.' }).click();



  // 2) add a title
  // await page.getByLabel('Title', { exact: true }).fill(WebcastEventTitle);

  // 3) Check that we can add date
  await page.getByRole('textbox', { name: 'Date :' }).fill(WebcastEventStartDate);
  await page.getByRole('textbox', { name: 'Time :' }).fill(WebcastEventStartHour);
  await page.getByRole('textbox', { name: 'Date:' }).fill(WebcastEventEndDate);
  await page.getByRole('textbox', { name: 'Time:' }).fill(WebcastEventEndHour);
  await page.getByLabel('Timezone').fill(WebcastEventTimeZone);

  // 4) Check that we can add category informations
  await page.getByLabel('Category', { exact: true }).fill(WebcastEventCategoryTitle);
  await page.getByLabel('Category ID').fill(WebcastEventCategoryID);

  // 5) Check that we can add webcast informations
  await page.getByLabel('Webcast link').fill(WebcastEventWebcastLink);
  await page.getByLabel('Webcast iCal').fill(WebcastEventWebcastIcal);

  // 6) Check that we can add a body
  await page.frameLocator('iframe[title="Rich Text Editor\\, Abstract field"]').locator('body').fill(WebcastEventAbstract);

  // 7) Check that we can publish a Webcast Event
  await page.getByRole('button', { name: 'Save' }).click();
  // await page.getByText('Webcast Event title has been created.').click();

  // 8) Check the published Webcast Event
  // await expect(page.getByRole('heading', { name: WebcastEventTitle }).locator('span')).toHaveText(WebcastEventTitle);
  await expect(page.getByText(WebcastEventAbstract)).toHaveText(WebcastEventAbstract);
  await expect(page.getByText(WebcastEventTimeZone)).toHaveText(WebcastEventTimeZone);
  await expect(page.getByText(WebcastEventCategoryTitle)).toHaveText(WebcastEventCategoryTitle);
  await expect(page.getByText(WebcastEventCategoryID)).toHaveText(WebcastEventCategoryID);
  await expect(page.getByText(WebcastEventWebcastLink)).toHaveText(WebcastEventWebcastLink);
  await expect(page.getByText(WebcastEventWebcastIcal)).toHaveText(WebcastEventWebcastIcal);
});
