import { test, expect } from '@playwright/test';

/* Test for "Basic page" content:
1) Check that we can open the creation page
2) Check that we can add a title
3) Check that we can add a body
4) Check that we can publish an basic page
5) Check the published basic page
*/

let basicPageTitle:string = 'this is a basic page title';
let basicPageBody:string = 'this is the basic page body';

test('test_content_basic_page', async ({ page }) => {
  // 1) open the creation page 
  await page.goto('/');
  
  await page.getByRole('link', { name: 'Content', exact: true }).click();
  await page.getByRole('link', { name: '+ Add content' }).click();
  await page.getByRole('link', { name: 'Basic page Use basic pages for your static content, such as an \'About us\' page.' }).click();

  // 2) add a title
  await page.getByLabel('Title', { exact: true }).fill(basicPageTitle);

  // 3) add a body
  await page.frameLocator('iframe[title="Rich Text Editor\\, Body field"]').locator('body').fill(basicPageBody);

  // 4) publish a basic page
  await page.getByRole('button', { name: 'Save' }).click();
  await page.getByLabel('Status message').isVisible();

  // 5) Check the published basic page
  await expect(page.getByRole('heading', { name: basicPageTitle }).locator('span')).toHaveText(basicPageTitle);
  await expect(page.getByText(basicPageBody)).toHaveText(basicPageBody);
});