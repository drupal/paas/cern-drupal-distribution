import { test as setup, expect } from '@playwright/test';

const authFile = 'playwright/.auth/user.json';

setup('authenticate', async ({ page}) => {

  // add this part because the button close of the dev message doesn't work
  await page.setViewportSize({ width: 1920, height: 1080 });

  await page.goto('/');
  await page.getByRole('link', { name: 'Sign in' }).click();
  await page.getByLabel('Username').fill(process.env.TEST_USER);
  await page.getByLabel('Password').fill(process.env.TEST_PASSWORD);
  await page.getByRole('button', { name: 'Sign In' }).click();


  await expect(page.getByRole('link', { name: 'Sign out' })).toBeVisible();

  // add this part because the button close of the dev message doesn't work
  await page.getByRole('link', { name: 'Configuration' }).click();
  await page.getByRole('link', { name: 'CERN dev-status Configurations for development sites' }).click();
  await page.getByLabel('Apply dev-status styling to website').uncheck();
  await page.getByRole('button', { name: 'Save configuration' }).click();

  await page.context().storageState({ path: authFile });
});