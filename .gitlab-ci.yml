stages:
  - environment
  - build
  - infrastructure tests
  - E2E tests
  - deprovision

.build_image: &build_image
  image:
    # The kaniko debug image is recommended because it has a shell, and a shell is required for an image to be used with GitLab CI/CD.
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
      # define script variables, they are passed in build time
    - IMAGE_DESTINATION=${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${TAG}
      # Prepare Kaniko configuration file
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
     # Build and push the image from the Dockerfile at the root of the project.
    - /kaniko/executor --context "$CI_PROJECT_DIR/$CONTEXT_DIR" --dockerfile "$CI_PROJECT_DIR/$CONTEXT_DIR/$DOCKERFILE" --destination "${IMAGE_DESTINATION}" $BUILD_ARGS
      # Print the full registry path of the pushed image
    - echo "Image pushed to ${IMAGE_DESTINATION}"

environment:
  stage: environment
  script:
    - |
      export DATE=$(date -u +%Y.%m.%dT%H-%M-%SZ);
      case "$CI_COMMIT_BRANCH" in
        v*) export TAG="${CI_COMMIT_BRANCH}-RELEASE-${DATE}" ;;
        master) export TAG="${CI_COMMIT_BRANCH}-RELEASE-${DATE}" ;;
        *) export TAG="${CI_COMMIT_BRANCH}-${CI_COMMIT_SHORT_SHA}" ;;
      esac
    - wget --no-check-certificate https://github.com/mikefarah/yq/releases/download/v4.2.0/yq_linux_amd64 -O /yq && chmod +x /yq
    - export softwareVersions=$CI_PROJECT_DIR/images/softwareVersions
    - export nginxVersion=`/yq e .nginx $softwareVersions`
    - export nginxNJSVersion=`/yq e .nginxNJS $softwareVersions`
    - export phpVersion=`/yq e .php $softwareVersions`
    - export composerVersion=`/yq e .composer $softwareVersions`
    - export composerBuilderTag=`/yq e .composerBuilderTag $softwareVersions`
    - echo "TAG=$TAG" >> env.env
    - echo "nginxVersion=$nginxVersion" >> env.env
    - echo "nginxNJSVersion=$nginxNJSVersion" >> env.env
    - echo "phpVersion=$phpVersion" >> env.env
    - echo "composerVersion=$composerVersion" >> env.env
    - echo "composerBuilderTag=$composerBuilderTag" >> env.env
    - echo "NAMESPACE=cern-drupal-distribution-ci" >> env.env
    - echo "PIPELINE_ID=${RANDOM}" >> env.env
  artifacts:
    reports:
      dotenv: env.env

build-sitebuilder:
  <<: *build_image
  stage: build
  needs: [environment]
  variables:
    IMAGE_NAME: 'site-builder'
    CONTEXT_DIR: 'images'
    DOCKERFILE: 'Dockerfile-sitebuilder'
    # space-separated list of variables to interpolate in the dockerfile
    BUILD_ARGS: '--build-arg COMPOSER_BUILDER_TAG=$composerBuilderTag'

build-composerbuilder:
  <<: *build_image
  stage: build
  needs: [environment]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - when: manual
      allow_failure: true
  variables:
    IMAGE_NAME: 'composer-builder'
    CONTEXT_DIR: 'images'
    DOCKERFILE: 'Dockerfile-composerbuilder'
    # space-separated list of variables to interpolate in the dockerfile
    BUILD_ARGS: '--build-arg PHP_VERSION=$phpVersion --build-arg COMPOSER_VERSION=$composerVersion --build-arg NGINX_VERSION=$nginxVersion --build-arg NJS_VERSION=$nginxNJSVersion'

infrastructure-test-newsite-provision:
  stage: infrastructure tests
  needs:
  - environment
  - build-sitebuilder
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:4
  variables:
    SITENAME: 'test-newsite-${PIPELINE_ID}'
  script:
      # Currently, all we require for all tests is to retrieve envsubst binary since it's not present on https://gitlab.cern.ch/paas-tools/openshift-client
      # Can be removed once https://gitlab.cern.ch/paas-tools/openshift-client/-/merge_requests/18#note_5241799 is resolved
    - curl -L --silent https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o /usr/local/bin/envsubst && chmod +x /usr/local/bin/envsubst
    - ./.gitlab/ci/scripts/test-new-site-creation.sh

infrastructure-test-clonesite-provision:
  stage: infrastructure tests
  needs:
  - environment
  - build-sitebuilder
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:4
  variables:
    SITENAME: 'test-clonesite-${PIPELINE_ID}'
  script:
      # Currently, all we require for all tests is to retrieve envsubst binary since it's not present on https://gitlab.cern.ch/paas-tools/openshift-client
      # Can be removed once https://gitlab.cern.ch/paas-tools/openshift-client/-/merge_requests/18#note_5241799 is resolved
    - curl -L --silent https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o /usr/local/bin/envsubst && chmod +x /usr/local/bin/envsubst
    - ./.gitlab/ci/scripts/test-clone-site-creation.sh

E2E-test-newsite-playwright:
  stage: E2E tests
  needs:
  - environment
  - infrastructure-test-newsite-provision
  image: mcr.microsoft.com/playwright:v1.39.0-jammy
  variables:
    SITENAME: 'test-newsite-${PIPELINE_ID}'
  script:
    - cd playwright-test
    - npm ci
    - export SITENAME=${SITENAME}
    - DEBUG=pw:api npx playwright test tests-newsite/
  artifacts:
        when: always
        paths:
            - playwright-test/playwright-report/
            - playwright-test/test-results/

E2E-test-clonesite-playwright:
  stage: E2E tests
  needs:
  - environment
  - infrastructure-test-clonesite-provision
  image: mcr.microsoft.com/playwright:v1.39.0-jammy
  variables:
    SITENAME: 'test-clonesite-${PIPELINE_ID}'
  script:
    - cd playwright-test
    - npm ci
    - export SITENAME=${SITENAME}
    - DEBUG=pw:api npx playwright test tests-clonesite/
  artifacts:
        when: always
        paths:
            - playwright-test/playwright-report/
            - playwright-test/test-results/


deprovision-test-resources:
  stage: deprovision
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:4
  script:
    - ./.gitlab/ci/scripts/deprovision-test-resources.sh

manual-deprovision-test-resources:
  stage: deprovision
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:4
  script:
    - ./.gitlab/ci/scripts/deprovision-test-resources.sh
  when: manual
